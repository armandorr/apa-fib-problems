s import matplotlib.pyplot as plt
from matplotlib.pyplot import figure
import random
import numpy as np

# Auxiliar function to obtain the 100 first primes numbers
def primes_upto(limit):
  prime = [True] * limit
  for n in range(2, limit):
    if prime[n]:
      yield n
      for c in range(n*n, limit, n):
        prime[c] = False

# Number of elements of the xl sequence
n = 100;

# X1,...,Xn sequence with the first 100 primes
xl = list(primes_upto(542));

# P1,...,Pn sequence with a uniform independent distribution
# Then divided by the sum of the sequence
p = [];
for i in range(n):
  p.append(random.uniform(0,1));

p = list(map(lambda elem: elem/sum(p),p));

# Main function of the problem
def f(x):
  sum = 0;
  for index, item in enumerate(xl):
    sum += p[index]*pow(x-item,2);
  return sum;

# Sample of x values
sample = np.linspace(0,500,100000);

# Result (f(x)) values
result = list(map(f,sample));

# Find minimum
def sumaPond():
  sum = 0;
  for index, item in enumerate(xl):
    sum += p[index]*item;
  return sum;

# Find f(minimum.x) to obtain the y value
X = sumaPond();
Y = f(X);

# Print result of minimum point
print("Minimum point");
print(" X:", X);
print(" Y:", Y);

# Plot graphic with minimum point
figure(figsize=(10, 8), dpi=80)
plt.scatter(X, Y, s=30, c="red")
plt.plot(sample, result)
plt.xlabel('X')
plt.ylabel('f(X)')
plt.title('Function f with 100 first prime sequence and uniform distribution of p')
plt.show()