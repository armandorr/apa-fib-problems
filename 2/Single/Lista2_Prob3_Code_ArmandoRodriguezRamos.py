import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_regression
from numpy import linalg as la

# Define maximum size and lambda
N = 1000;
lamb = np.random.random()*10;

# Make regression
n_samples = np.random.randint(N/10, N);
n_features = np.random.randint(N/10, N);
n_informative = np.random.randint(0, n_features);
bias = np.random.random()*10;
noise = np.random.random()*10;
rng = np.random.randint(0, N);

X, Yt, wUnderlying = make_regression(n_samples=n_samples, n_features=n_features, 
                                     n_informative=n_informative, bias=bias, 
                                     noise=noise, coef=True, random_state=rng);

# Show initialization parameters
print("Regression information:");
print("   n_samples:", n_samples);
print("   n_features:", n_features);
print("   n_informative:", n_informative);
print("   bias:", bias);
print("   noise:", noise);
print("   rng_state:", rng, end = '\n\n');
print("lambda:", lamb, end = '\n\n')


# Sizes of the problem
m = X.shape[0];
n = X.shape[1];


# Ridge weights
Xt = np.copy(X).transpose();
Yt = np.matrix(Yt);
Y = np.copy(Yt).transpose();
I = np.identity(n_features);

w1 = np.matmul(np.linalg.inv(np.matmul(Xt,X)+lamb*I),np.matmul(Xt,Y));
w1t = np.copy(w1).transpose();


# Ridge with SVD weights
U, s, Vt = la.svd(X,full_matrices=False);

Ut = np.copy(U).transpose();
V = np.copy(Vt).transpose();
S = np.diag(s);
S2 = S*S;
I = np.identity(min(m,n));

w2 = np.matmul(V,np.matmul(np.linalg.inv(S2+lamb*I),np.matmul(S,np.matmul(Ut,Y))));
w2t = np.copy(w2).transpose();


# Underlying weights
wUnderlying = np.reshape(wUnderlying, (n_features,1))
wUnderlyingt = np.copy(wUnderlying).transpose();


# Show results
def error(x,y,w):
    return np.max(abs((np.full((1,m),bias)+np.matmul(w,x))-y));

print("Single results:")
print("Ridge:")
print("   Max weight value:",np.max(w1))
print("   Max simple error:",error(Xt,Yt,w1t));
print("Ridge SVD:")
print("   Max weight value:",np.max(w2))
print("   Max simple error:",error(Xt,Yt,w2t));
print("Underlying:")
print("   Max weight value:",np.max(wUnderlyingt))
print("   Max simple error:",error(Xt,Yt,wUnderlyingt),end="\n\n");
print("Weights comparisons:")
print("Ridge vs Ridge SVD:")
print("   Weights are close?",np.allclose(w1,w2));
print("   Maximum difference:",max(abs(w1-w2)), end="\n\n");
print("Ridge vs Underlying:")
print("   Weights are close?",np.allclose(w1,wUnderlying));
print("   Maximum difference:",max(abs(w1-wUnderlying)), end="\n\n");
print("Ridge SVD vs Underlying:")
print("   Weights are close?",np.allclose(w2,wUnderlying));
print("   Maximum difference:",max(abs(w2-wUnderlying)), end="\n\n");


# Auxiliar functions to plot
def f1(w):
  return lambda x : bias+w.item(0)*x;

def f2(w):
  return lambda x,y : bias+w.item(0)*x+w.item(1)*y;


# Plots to perform if possible
if n_features == 1:
  sample = np.linspace(-5,5,N);
  resultW1 = list(map(f1(w1),sample));
  resultW2 = list(map(f1(w2),sample));
  resultUnderlying = list(map(f1(wUnderlying),sample));
  plt.scatter(X,Y);
  plt.plot(sample,resultW1);
  plt.plot(sample,resultW2);
  plt.plot(sample,resultUnderlying);
  plt.show();

if n_features == 2:
  sampleX = np.linspace(-5,5,N);
  sampleY = np.linspace(-5,5,N);
  resultW1 = list(map(f2(w1),sampleX,sampleY));
  resultW2 = list(map(f2(w2),sampleX,sampleY));
  resultUnderlying = list(map(f2(wUnderlying),sampleX,sampleY));
  ax = plt.axes(projection='3d');
  ax.scatter3D(X[:,0],X[:,1],Yt);
  ax.plot3D(sampleX, sampleY, resultW1);
  ax.plot3D(sampleX, sampleY, resultW2);
  ax.plot3D(sampleX, sampleY, resultUnderlying);
  plt.show();